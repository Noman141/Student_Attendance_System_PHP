// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

$('document').ready(function(){
   $('form').submit(function(){
     var roll = true;
     $(':radio').each(function(){
        name = $(this).attr('name');
           if (roll && !$(':radio[name = "' + name + '"]:checked').length) {
            //alert(name + "Roll Messing!");
            $('.alert').show();
            roll = false;
           }
     });
     return roll;
   });
});