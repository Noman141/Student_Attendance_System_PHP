<?php 
include "inc/header.php";
include "classes/Student.php";
?>
<?php
$stu = new Student();
$date = $_GET['date'];
?>
  <div class="card">
    <div class="card-header pt-3 d-flex justify-content-between" style="font-size: 20px">
      <a class="btn btn-info" href="add.php">Add Student</a>
      <span><strong>Attendance Date :- </strong><?php echo  $date ;?></span>
      <a class="btn btn-info" href="view.php">Back</a>
    </div>
    <div class="card-body">
        <?php
            if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                $attend = $_POST['attend'];
                $att_update = $stu->updateAttendance($date,$attend);
                if ($att_update){
                    echo $att_update;
                }
           }
        ?>
      <form action="" method="post">
         <table class="table table-striped">
           <tr>
             <th width="25%">Serial</th>
             <th width="25%">Name</th>
             <th width="25%">Student Roll</th>
             <th width="25%">Attendance</th>
           </tr>

           <tr>
               <?php
                  $getstudent = $stu->getAllStudentData($date);
                  $i = 0;
                  if ($getstudent){
                      while ($studentsdata = $getstudent->fetch_assoc()){
                          $i++;
               ?>
             <td><?php echo $i;?></td>
             <td><?php echo $studentsdata['name'];?></td>
             <td><?php echo $studentsdata['roll'];?></td>
             <td>
               <input type="radio" name="attend[<?php echo $studentsdata['roll'];?>]" value="present"<?php if ($studentsdata["attend"] == "present") {echo "checked";} ;?>>P ||
               <input type="radio" name="attend[<?php echo $studentsdata['roll'];?>]" value="absent" <?php if ($studentsdata["attend"] == "absent") {echo "checked";} ;?>>A
             </td>
           </tr>
         <?php  } }?>
         <tr>
            <td></td>
             <td></td>
             <td></td>
             <td>
               <input type="submit" name="submit" class="btn btn-info" value="Update">
             </td>
           </tr>
         </table>
      </form>
    </div>
  </div>

<?php include "inc/footer.php" ?>