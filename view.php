<?php 
include "inc/header.php";
include "classes/Student.php";
?>
  <div class="card">
    <div class="card-header pt-3 d-flex justify-content-between" style="font-size: 20px">
      <a class="btn btn-info" href="add.php">Add Student</a>
      <a class="btn btn-info" href="index.php">Take Attendance</a>
    </div>
    <div class="card-body">
      <form action="" method="post">
         <table class="table table-striped">
           <tr>
             <th width="20%">Serial</th>
             <th width="50%">Attendance Date</th>
             <th width="30%">Action</th>
           </tr>

           <tr>
               <?php
                  $stu = new Student();
                  $getDate = $stu->getDateList();
                  $i = 0;
                  if ($getDate){
                      while ($getData = $getDate->fetch_assoc()){
                          $i++;
               ?>
             <td><?php echo $i;?></td>
             <td><?php echo $getData['att_time'];?></td>
             <td>
               <a class="btn btn-info" href="student_view.php?date=<?php echo $getData['att_time'];?>">View</a>
             </td>
           </tr>
         <?php } }?>
         </table>
      </form>
    </div>
  </div>

<?php include "inc/footer.php" ?>