<?php
require_once "lib/Database.php";
class Student{
    private $db;
    public function __construct(){
        $this->db = new Database();
    }

    public function getAllStudent(){
        $sql = "SELECT * FROM tbl_student ORDER BY id ASC ";
        $getstudent = $this->db->select($sql);
        return $getstudent;
    }

    public function addStudent(){
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            $name = $_POST['name'];
            $roll = $_POST['roll'];

            $name = mysqli_real_escape_string($this->db->link,$name);
            $roll = mysqli_real_escape_string($this->db->link,$roll);

            if ($name ==''){
                $msg = "<span class='alert alert-danger'>Please Enter Student Name</span>";
                return $msg;
            }elseif ($roll==''){
                $msg = "<span class='alert alert-danger'>Please Enter Student Roll</span>";
                return $msg;
            }else{
                $rollquery = "SELECT * FROM tbl_student WHERE roll = '$roll' LIMIT 1";
                $rollcheck = $this->db->select($rollquery);
                if ($rollcheck != false) {
                    $msg = "<span class='alert alert-danger'>This Roll Number Is Already Exsist</span>";
                    return $msg;
                }else{
                    $sql = "INSERT INTO tbl_student(name,roll) VALUES ('$name','$roll')";
                    $inserted_row = $this->db->insert($sql);
                    $sql = "INSERT INTO tbl_attend(roll) VALUES ('$roll')";
                    $inserted_row = $this->db->insert($sql);
                    if ($inserted_row){
                        $msg = "<span class='alert alert-success'>Sutdent Added Successfull</span>";
                        return $msg;
                    }else{
                        $msg = "<span class='alert alert-danger'>Sutdent Not Added</span>";
                        return $msg;
                    }
                }
            }
        }
    }

    public function insertAttendance(){
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            $attend = $_POST['attend'];
            $crnt_date = date("Y-m-d");

                $sql = "SELECT DISTINCT att_time FROM tbl_attend ORDER BY id DESC";
                $getdata = $this->db->select($sql);
                if ($getdata){
                    while ($get_time = $getdata->fetch_assoc()){
                        $db_date = $get_time['att_time'];
                        if ($crnt_date != $db_date){
                            foreach ($attend as $atn_key => $atn_value){
                                if ($atn_value == 'present'){
                                    $sql = "INSERT INTO tbl_attend(roll,attend,att_time) VALUES('$atn_key','present','$crnt_date')";
                                    $data_insert = $this->db->insert($sql);
                                }elseif ($atn_value == 'absent'){
                                    $sql = "INSERT INTO tbl_attend(roll,attend,att_time) VALUES('$atn_key','absent','$crnt_date')";
                                    $data_insert = $this->db->insert($sql);
                                }
                            }
                            if ($data_insert){
                                $msg = "<span class='alert alert-success'>Attendance Data Inserted</span>";
                                return $msg;
                            }else{
                                $msg = "<span class='alert alert-danger'>Attendance Data Not Inserted</span>";
                                return $msg;
                            }
                        }else{
                            $msg = "<span class='alert alert-danger'>Today's Attendance Already Taken</span>";
                            return $msg;
                        }
                    }
                }

            }
    }

    public function getDateList(){
        $sql = "SELECT DISTINCT att_time FROM tbl_attend ORDER BY id DESC";
        $getdate = $this->db->select($sql);
        return $getdate;
    }

    public function getAllStudentData($date){
        $sql = "SELECT tbl_student.name, tbl_attend.*
                FROM tbl_student
                INNER JOIN tbl_attend
                ON tbl_student.roll = tbl_attend.roll
                WHERE att_time = '$date'";
        $stmt = $this->db->select($sql);
        return $stmt;
    }

    public function updateAttendance($date,$attend){
       foreach ($attend as $att_key=>$att_value){
           if ($att_value == 'present'){
               $sql = "UPDATE tbl_attend
                       SET 
                       attend = 'present'
                       WHERE roll = '".$att_key."' AND 	att_time = '".$date."'";
               $data_update = $this->db->update($sql);
           }elseif ($att_value == 'absent'){
               $sql = "UPDATE tbl_attend
                       SET 
                       attend = 'absent'
                       WHERE roll = '".$att_key."' AND 	att_time = '".$date."'";
               $data_update = $this->db->update($sql);
           }
       }

        if ($data_update){
            $msg = "<span class='alert alert-success'>Attendance Data Updated</span>";
            return $msg;
        }else{
            $msg = "<span class='alert alert-danger'>Attendance Data Not Updated</span>";
            return $msg;
        }
    }
}