<?php 
include "inc/header.php";
include "classes/Student.php";
?>
<?php
$stu = new Student();
$crnt_date = date("Y-m-d");
?>

  <div class="card">
    <div class="card-header pt-3 d-flex justify-content-between" style="font-size: 20px">
      <a class="btn btn-info" href="add.php">Add Student</a>
      <span><strong>Today's Date Is :- </strong><?php echo  $crnt_date ;?></span>
      <a class="btn btn-info" href="view.php">View All</a>
    </div>
    <div class="card-body">
        <?php
           $stu = new  Student();
           $takeattendence = $stu->insertAttendance();
           if ($takeattendence){
               echo $takeattendence;
           }
        ?>
      <span class='alert alert-danger mb-5' style="display: none;">Student Roll Messing</span>
      <form action="" method="post">
         <table class="table table-striped mt-3">
           <tr>
             <th width="25%">Serial</th>
             <th width="25%">Name</th>
             <th width="25%">Student Roll</th>
             <th width="25%">Attendance</th>
           </tr>

           <tr>
               <?php
                  $student = $stu->getAllStudent();
                  $i = 0;
                  if ($student){
                      while ($students = $student->fetch_assoc()){
                          $i++;
               ?>
             <td><?php echo $i;?></td>
             <td><?php echo $students['name'];?></td>
             <td><?php echo $students['roll'];?></td>
             <td>
               <input type="radio" name="attend[<?php echo $students['roll'];?>]" value="present">P ||
               <input type="radio" name="attend[<?php echo $students['roll'];?>]" value="absent">A
             </td>
           </tr>
         <?php } }?>
           <tr>
            <td></td>
             <td></td>
             <td></td>
             <td>
               <input type="submit" name="submit" class="btn btn-info" value="Submit">
             </td>
           </tr>
         </table>
      </form>
    </div>
  </div>

<?php include "inc/footer.php" ?>