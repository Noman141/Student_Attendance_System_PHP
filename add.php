<?php 
include "inc/header.php";
include "classes/Student.php";
?>
<?php
  $stu = new Student();
?>

  <div class="card">
    <div class="card-header pt-3 d-flex justify-content-between" style="font-size: 20px">
      <a class="btn btn-info" href="view.php">View Student List</a>
      <a class="btn btn-info" href="index.php">Back</a>
    </div>
    <div class="card-body" style="width: 700px;margin: 0 auto">
      <form action="" method="post" class="needs-validation" novalidate>
          <?php
             $addstudent = $stu->addStudent();

             if ($addstudent){
                 echo $addstudent;
             }
          ?>
        <div class="form-group row mt-5">
          <label for="name" class="col-md-3 col-form-label">Student Name</label>
          <div class="col-md-9">
            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Student Name" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="roll" class="col-md-3 col-form-label">Roll</label>
          <div class="col-md-9">
            <input type="text" class="form-control" name="roll" id="roll" placeholder="Enter Roll" required>
          </div>
        </div>
        <div class="form-group row">
            <input type="submit" class="btn btn-info" value="Submit" style="margin-left: 290px;">
        </div>
      </form>
    </div>
  </div>

          <script>

            </script>

<?php include "inc/footer.php" ?>