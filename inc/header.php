<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <title>Student/Employee Attendance System PHP</title>
  </head>
  <body>
    <div class="pb-5">
      <div class="container">
        <div class="text-center p-3 bg-secondary text-light">
          <h2>Student/Employee Attendance System PHP</h2>
        </div>